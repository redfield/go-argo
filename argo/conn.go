// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package argo

import (
	"net"
	"net/url"
	"os"
	"strconv"
	"time"
)

// Argo protocol type, either DGRAM or STREAM.
type protocolType uint

const (
	// dgram  protocolType = 1
	stream protocolType = 2
)

func DialStream(target string) (*Conn, error) {
	addr, err := parseAddr(target)
	if err != nil {
		return nil, err
	}

	file, err := os.OpenFile("/dev/argo_stream", os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}

	c := &Conn{
		file:  file,
		ptype: stream,
		raddr: addr,
	}

	_, err = ioctl(c.file.Fd(), iocConnect, addr.bytes())
	if err != nil {
		c.Close()

		return nil, err
	}

	return c, nil
}

func parseAddr(target string) (*XenAddr, error) {
	// Allow target to include or not include
	// the argo:// scheme explicitly.
	u, err := url.Parse(target)
	if err != nil {
		u, err = url.Parse("argo://" + target)
		if err != nil {
			return nil, err
		}
	}

	domid, err := strconv.Atoi(u.Hostname())
	if err != nil {
		return nil, err
	}

	port, err := strconv.Atoi(u.Port())
	if err != nil {
		return nil, err
	}

	addr := &XenAddr{
		Domid: uint16(domid),
		Port:  uint32(port),
	}

	return addr, nil
}

type Conn struct {
	// For send and recv are implemented as file operations,
	// so we'll use os.File to easily implement the majority
	// of the net.Conn interface.
	file  *os.File
	ptype protocolType

	laddr *XenAddr
	raddr *XenAddr
}

func (c *Conn) Read(b []byte) (n int, err error) {
	return c.file.Read(b)
}

func (c *Conn) Write(b []byte) (n int, err error) {
	return c.file.Write(b)
}

func (c *Conn) Close() error {
	return c.file.Close()
}

func (c *Conn) LocalAddr() net.Addr {
	return c.laddr
}

func (c *Conn) RemoteAddr() net.Addr {
	return c.raddr
}

func (c *Conn) SetDeadline(t time.Time) error {
	return c.file.SetDeadline(t)
}

func (c *Conn) SetReadDeadline(t time.Time) error {
	return c.file.SetReadDeadline(t)
}

func (c *Conn) SetWriteDeadline(t time.Time) error {
	return c.file.SetWriteDeadline(t)
}
